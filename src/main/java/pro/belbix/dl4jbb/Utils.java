package pro.belbix.dl4jbb;

import org.deeplearning4j.datasets.iterator.impl.MnistDataSetIterator;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.SplitTestAndTrain;
import org.nd4j.linalg.dataset.api.DataSet;
import org.nd4j.linalg.factory.Nd4j;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Utils {


    public static UnsupervisedSet mnistUnsupervisedSet() throws IOException {
        //Load data and split into training and testing sets. 40000 train, 10000 test
        MnistDataSetIterator iter = new MnistDataSetIterator(100, 50000, false);

        List<INDArray> featuresTrain = new ArrayList<INDArray>();
        List<INDArray> featuresTest = new ArrayList<INDArray>();
        List<INDArray> labelsTest = new ArrayList<INDArray>();

        Random rand = new Random(12345);

        while (iter.hasNext()) {
            DataSet next = iter.next();
            SplitTestAndTrain split = next.splitTestAndTrain(80, rand);  //80/20 split (from miniBatch = 100)
            featuresTrain.add(split.getTrain().getFeatures());
            DataSet dsTest = split.getTest();
            featuresTest.add(dsTest.getFeatures());
            INDArray indexes = Nd4j.argMax(dsTest.getLabels(), 1); //Convert from one-hot representation -> index
            labelsTest.add(indexes);
//            labelsTest.add(dsTest.getLabels());
        }
        return new UnsupervisedSet(featuresTrain, featuresTest, labelsTest);
    }

}
