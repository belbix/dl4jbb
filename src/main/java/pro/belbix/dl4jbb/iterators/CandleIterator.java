package pro.belbix.dl4jbb.iterators;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.*;

public class CandleIterator implements DataSetIterator {

    private float[] validPrices;
    private TreeMap<Float, Integer> priceToIdxMap;
    private float[] filePrices;
    private int exampleLength;
    private int miniBatchSize;
    private Random rng;
    private LinkedList<Integer> exampleStartOffsets = new LinkedList<>();

    public CandleIterator(String textFilePath, Charset textFileEncoding,
                          int miniBatchSize, int exampleLength, Random rng, int priceAccuracy) throws IOException {
        if (!new File(textFilePath).exists())
            throw new IOException("Could not access file (does not exist): " + textFilePath);
        if (miniBatchSize <= 0) throw new IllegalArgumentException("Invalid miniBatchSize (must be >0)");
        this.exampleLength = exampleLength;
        this.miniBatchSize = miniBatchSize;
        this.rng = rng;

        priceToIdxMap = initPriceToIdx(2000, 6000);

        List<String> lines = Files.readAllLines(new File(textFilePath).toPath(), textFileEncoding);

        int maxSize = lines.size();
        for (String s : lines) maxSize += s.length();
        float[] prices = new float[maxSize];
        int currIdx = 0;
        for (String s : lines) {
            String[] thisLine = s.split(",");
            for (String sPrice : thisLine) {
                float price = Float.parseFloat(sPrice);
                priceToIdxGetKey(price);
                prices[currIdx++] = price;
            }
//            if (newLineValid) prices[currIdx++] = '\n';
        }

        if (currIdx == prices.length) {
            filePrices = prices;
        } else {
            filePrices = Arrays.copyOfRange(prices, 0, currIdx);
        }
        if (exampleLength >= filePrices.length)
            throw new IllegalArgumentException("exampleLength=" + exampleLength
                    + " cannot exceed number of valid prices in file (" + filePrices.length + ")");

        System.out.println("Loaded and converted file: " + filePrices.length + " valid characters");

        initializeOffsets();
    }
    public TreeMap<Float, Integer> initPriceToIdx(float min, float max) {
        return initPriceToIdx(min, max, Math.round(max - min));
    }

    public TreeMap<Float, Integer> initPriceToIdx(float min, float max, int count) {
        TreeMap<Float, Integer> ret = new TreeMap<>();
        validPrices = new float[count];
        float delta = max - min;
        float batchSize = delta / (float) count;
        for (int i = 0; i < count; i++) {
            float el = batchSize * (i + 1);
            ret.put(el, i);
            validPrices[i] = el;
        }
        return ret;
    }

    private void initializeOffsets() {
        //This defines the order in which parts of the file are fetched
        int nMinibatchesPerEpoch = (filePrices.length - 1) / exampleLength - 2;   //-2: for end index, and for partial example
        if (nMinibatchesPerEpoch < 1)
            throw new RuntimeException("Too small data " + filePrices.length + " for exampleLength " + exampleLength + "" +
                    ". Need modulo + 2 > 1");
        for (int i = 0; i < nMinibatchesPerEpoch; i++) {
            exampleStartOffsets.add(i * exampleLength);
        }
        Collections.shuffle(exampleStartOffsets, rng);
    }


    @Override
    public DataSet next(int num) {
        if (exampleStartOffsets.size() == 0) throw new NoSuchElementException();

        int currMinibatchSize = Math.min(num, exampleStartOffsets.size());
        //Allocate space:
        //Note the order here:
        // dimension 0 = number of examples in minibatch
        // dimension 1 = size of each vector (i.e., number of characters)
        // dimension 2 = length of each time series/example
        //Why 'f' order here? See http://deeplearning4j.org/usingrnns.html#data section "Alternative: Implementing a custom DataSetIterator"
        INDArray input = Nd4j.create(new int[]{currMinibatchSize, priceToIdxMap.size(), exampleLength}, 'f');
        INDArray labels = Nd4j.create(new int[]{currMinibatchSize, priceToIdxMap.size(), exampleLength}, 'f');

        for (int i = 0; i < currMinibatchSize; i++) {
            int startIdx = exampleStartOffsets.removeFirst();
            int endIdx = startIdx + exampleLength;
            float currPrice = filePrices[startIdx];
            int currPriceIdx = priceToIdxMap.get(priceToIdxGetKey(currPrice));    //Current input
            int c = 0;
            for (int j = startIdx + 1; j < endIdx; j++, c++) {
                float nextPrice = filePrices[j];
                int nextPriceIdx = priceToIdxMap.get(priceToIdxGetKey(nextPrice));        //Next character to predict
                input.putScalar(new int[]{i, currPriceIdx, c}, 1.0);
                labels.putScalar(new int[]{i, nextPriceIdx, c}, 1.0);
                currPriceIdx = nextPriceIdx;
            }
        }

        return new DataSet(input, labels);
    }

    private float priceToIdxGetKey(float price) {
        if (price < priceToIdxMap.firstKey())
            throw new RuntimeException("Invalid price " + price);
        float lastKey = 0;
        for (float key : priceToIdxMap.keySet()) {
            if (price < key) {
                break;
            }
            lastKey = key;
        }
        if (lastKey == 0)
            throw new RuntimeException("Invalid price " + price);
        return lastKey;
    }

    public float convertIndexToPrice(int idx) {
        return validPrices[idx];
    }

    public int convertPriceToIndex(float c) {
        return priceToIdxMap.get(priceToIdxGetKey(c));
    }

    public float getRandomPrice() {
        return validPrices[(int) (rng.nextDouble() * validPrices.length)];
    }

    public int totalExamples() {
        return (filePrices.length - 1) / miniBatchSize - 2;
    }

    public int cursor() {
        return totalExamples() - exampleStartOffsets.size();
    }

    public int numExamples() {
        return totalExamples();
    }

    @Override
    public int inputColumns() {
        return validPrices.length;
    }

    @Override
    public int totalOutcomes() {
        return validPrices.length;
    }

    @Override
    public boolean resetSupported() {
        return true;
    }

    @Override
    public boolean asyncSupported() {
        return true;
    }

    @Override
    public void reset() {
        exampleStartOffsets.clear();
        initializeOffsets();
    }

    @Override
    public int batch() {
        return miniBatchSize;
    }

    @Override
    public void setPreProcessor(DataSetPreProcessor preProcessor) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public DataSetPreProcessor getPreProcessor() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public List<String> getLabels() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean hasNext() {
        return exampleStartOffsets.size() > 0;
    }

    @Override
    public DataSet next() {
        return next(miniBatchSize);
    }
}
