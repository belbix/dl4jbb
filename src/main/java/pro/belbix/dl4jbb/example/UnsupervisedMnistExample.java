package pro.belbix.dl4jbb.example;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.common.primitives.ImmutablePair;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.learning.config.AdaGrad;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import pro.belbix.dl4jbb.UnsupervisedSet;

import java.io.IOException;
import java.util.*;

import static pro.belbix.dl4jbb.Utils.mnistUnsupervisedSet;

@Slf4j
public class UnsupervisedMnistExample {


    public static void main(String[] args) throws IOException {
        val conf = new NeuralNetConfiguration.Builder()
                .seed(12345)
                .weightInit(WeightInit.XAVIER)
                .updater(new AdaGrad(0.05))
                .activation(Activation.RELU)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .l2(0.0001)
                .list()
                .layer(0, new DenseLayer.Builder().nIn(784).nOut(250)
                        .build())
                .layer(1, new DenseLayer.Builder().nIn(250).nOut(10)
                        .build())
                .layer(2, new DenseLayer.Builder().nIn(10).nOut(250)
                        .build())
                .layer(3, new OutputLayer.Builder().nIn(250).nOut(784)
                        .lossFunction(LossFunctions.LossFunction.MSE)
                        .build())
                .build();

        val net = new MultiLayerNetwork(conf);
        net.setListeners(new ScoreIterationListener(1000));


        UnsupervisedSet set = mnistUnsupervisedSet();
        List<INDArray> featuresTrain = set.getFeaturesTrain();
        List<INDArray> featuresTest = set.getFeaturesTest();
        List<INDArray> labelsTest = set.getLabelsTest();


        // the "simple" way to do multiple epochs is to wrap fit() in a loop
        val nEpochs = 1;
        for (int epoch = 1; epoch <= nEpochs; epoch++) {
            featuresTrain.forEach(data -> net.fit(data, data));
            log.info("Epoch " + epoch + " complete");
        }


        //Evaluate the model on the test data
        //Score each example in the test set separately
        //Compose a map that relates each digit to a list of (score, example) pairs
        //Then find N best and N worst scores per digit
        val listsByDigit = new HashMap<Integer, ArrayList<ImmutablePair<Double, INDArray>>>();

        for (int i = 0; i < 10; i++) {
            listsByDigit.put(i, new ArrayList<ImmutablePair<Double, INDArray>>());
        }
        for (int i = 0; i < featuresTest.size(); i++) {
            val testData = featuresTest.get(i);
            val labels = labelsTest.get(i);
            for (int j = 0; j < testData.rows(); j++) {
                val example = testData.getRow(j);
                val digit = (int) labels.getDouble(j);
                val score = net.score(new DataSet(example, example));
                // Add (score, example) pair to the appropriate list
                val digitAllPairs = listsByDigit.get(digit);
                digitAllPairs.add(new ImmutablePair<Double, INDArray>(score, example));
            }
        }


        //Sort each list in the map by score
        val c = new Comparator<ImmutablePair<Double, INDArray>>() {
            @Override
            public int compare(ImmutablePair<Double, INDArray> o1, ImmutablePair<Double, INDArray> o2) {
                return Double.compare(o1.getLeft(), o2.getLeft());
            }
        };

        listsByDigit.values().forEach(digitAllPairs -> Collections.sort(digitAllPairs, c));

        //After sorting, select N best and N worst scores (by reconstruction error) for each digit, where N=5
        val best = new ArrayList<INDArray>(50);
        val worst = new ArrayList<INDArray>(50);

        for (int i = 0; i < 10; i++) {
            val list = listsByDigit.get(i);

            for (int j = 0; j < 4; j++) {
                best.add(list.get(j).getRight());
                worst.add(list.get(list.size() - j - 1).getRight());
            }
        }
        log.info(best.toString());
        log.info(worst.toString());

    }

}
