package pro.belbix.dl4jbb.example;


import lombok.extern.slf4j.Slf4j;
import org.deeplearning4j.datasets.iterator.impl.EmnistDataSetIterator;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.evaluation.classification.Evaluation;
import org.nd4j.evaluation.classification.ROCMultiClass;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.IOException;

@Slf4j
public class MNIST {

    public static void main(String[] args) throws IOException {
        int batchSize = 128; // how many examples to simultaneously train in the network
        EmnistDataSetIterator.Set emnistSet = EmnistDataSetIterator.Set.BALANCED;
        EmnistDataSetIterator emnistTrain = new EmnistDataSetIterator(emnistSet, batchSize, true);
        EmnistDataSetIterator emnistTest = new EmnistDataSetIterator(emnistSet, batchSize, true);

        int outputNum = EmnistDataSetIterator.numLabels(emnistSet); // total output classes
        int rngSeed = 123; // integer for reproducability of a random number generator
        int numRows = 28; // number of "pixel rows" in an mnist digit
        int numColumns = 28;

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(rngSeed)
                .updater(new Adam())
                .l2(1e-4)
                .list()
                .layer(new DenseLayer.Builder()
                        .nIn(numRows * numColumns) // Number of input datapoints.
                        .nOut(1000) // Number of output datapoints.
                        .activation(Activation.RELU) // Activation function.
                        .weightInit(WeightInit.XAVIER) // Weight initialization.
                        .build())
                .layer(new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .nIn(1000)
                        .nOut(outputNum)
                        .activation(Activation.SOFTMAX)
                        .weightInit(WeightInit.XAVIER)
                        .build())
                .build();

        // create the MLN
        MultiLayerNetwork network = new MultiLayerNetwork(conf);
        network.init();

        // pass a training listener that reports score every 10 iterations
        int eachIterations = 100;
        network.addListeners(new ScoreIterationListener(eachIterations));

        // fit for multiple epochs
        int numEpochs = 2;
//        network.fit(emnistTrain, numEpochs);

        evaluate(network, emnistTest, 0);
    }

    private static void evaluate(MultiLayerNetwork network, EmnistDataSetIterator emnistTest, int classIndex) {
        Evaluation eval = network.evaluate(emnistTest);

        log.info(eval.accuracy() + "");
        log.info(eval.precision() + "");
        log.info(eval.recall() + "");

        // evaluate ROC and calculate the Area Under Curve
        ROCMultiClass roc = network.evaluateROCMultiClass(emnistTest, 0);
        roc.calculateAUC(classIndex);

        // optionally, you can print all stats from the evaluations
        log.info(eval.stats());
        log.info(roc.stats());
    }

}
