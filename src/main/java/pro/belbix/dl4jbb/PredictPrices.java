package pro.belbix.dl4jbb;

import org.deeplearning4j.nn.api.Layer;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.LSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import pro.belbix.dl4jbb.iterators.CandleIterator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@SuppressWarnings("ALL")
public class PredictPrices {

    private static final int PRICE_IN_LINE = 500;
    private static final String SEPARATOR = ",";
    private static final String ROOT_PATH = "/trainsets/candles/";
    private static final String DATA = "bitmexXBTUSD1.txt";

    public static void main(String[] args) throws Exception {
        String pathForSaveNet = ROOT_PATH + "net";
        int priceAccuracy = 1000;
        int lstmLayerSize = 1000;                    //Number of units in each LSTM layer
        int miniBatchSize = 32;                        //Size of mini batch to use when  training
        int exampleLength = 100;                    //Length of each training example sequence to use. This could certainly be increased
        int tbpttLength = 50;                       //Length for truncated backpropagation through time. i.e., do parameter updates ever 50 characters
        int numEpochs = 100;                            //Total number of training epochs
        int generateSamplesEveryNMinibatches = 10;  //How frequently to generate samples from the network? 1000 characters / 50 tbptt length: 20 parameter updates per minibatch
        int nSamplesToGenerate = 1;                    //Number of samples to generate after each training epoch
        int nPricesToSample = 10;                //Length of each sample to generate
        float[] generationInitialization = pricesForPrediction();        //Optional character initialization; a random character is used if null
        // Above is Used to 'prime' the LSTM with a character sequence to continue/complete.
        // Initialization characters must all be in CharacterIterator.getMinimalCharacterSet() by default
        Random rng = new Random(12345);

        //Get a DataSetIterator that handles vectorization of text into something we can use to train
        // our LSTM network.
        CandleIterator iter = getCandleIterator(miniBatchSize, exampleLength, priceAccuracy);
        int nOut = iter.totalOutcomes();
        int nIn = iter.inputColumns();

        //Set up network configuration:
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(12345)
                .l2(0.0001)
                .weightInit(WeightInit.XAVIER)
                .updater(new Adam(0.005))
                .list()
                .layer(0, new LSTM.Builder().nIn(nIn).nOut(lstmLayerSize)
                        .activation(Activation.TANH).build())
                .layer(1, new LSTM.Builder().nIn(lstmLayerSize).nOut(lstmLayerSize)
                        .activation(Activation.TANH).build())
                .layer(2, new RnnOutputLayer.Builder(
                        LossFunctions.LossFunction.MCXENT).activation(Activation.SOFTMAX)
                        .nIn(lstmLayerSize).nOut(nOut).build())
                .backpropType(BackpropType.TruncatedBPTT)
                .tBPTTForwardLength(tbpttLength)
                .tBPTTBackwardLength(tbpttLength)
                .build();

        MultiLayerNetwork net = new MultiLayerNetwork(conf);

        File fNet = new File(pathForSaveNet);
        if (fNet.exists()) {
            net = net.load(fNet, true);
            System.out.println("Net loaded from " + fNet);
        } else {
            net.init();
        }
        net.setListeners(new ScoreIterationListener(1));

        //Print the  number of parameters in the network (and for each layer)
        Layer[] layers = net.getLayers();
        long totalNumParams = 0;
        for (int i = 0; i < layers.length; i++) {
            long nParams = layers[i].numParams();
            System.out.println("Number of parameters in layer " + i + ": " + nParams);
            totalNumParams += nParams;
        }
        System.out.println("Total number of network parameters: " + totalNumParams);

        //Do training, and then generate and print samples from network
        int miniBatchNumber = 0;
        for (int i = 0; i < numEpochs; i++) {
            while (iter.hasNext()) {
                DataSet ds = iter.next();
                net.fit(ds);
                if (++miniBatchNumber % generateSamplesEveryNMinibatches == 0) {
                    samples(generationInitialization, net, iter, rng, nPricesToSample, nSamplesToGenerate);
                }
            }
            iter.reset();    //Reset iterator for another epoch
            net.save(fNet);
        }

        System.out.println("\n\nExample complete");
    }

    private static void samples(float[] initialization, MultiLayerNetwork net,
                                CandleIterator iter, Random rng, int pricesToSample, int numSamples) {
        System.out.println("--------------------");
        String[] samples = samplePricesFromNetwork(
                initialization, net, iter, rng, pricesToSample, numSamples);
        for (int j = 0; j < samples.length; j++) {
            System.out.println("----- Sample " + j + " -----");
            String result = samples[j];
            saveResult(result);
            System.out.println(result);
            System.out.println();
        }
    }

    private static void saveResult(String result) {
        String[] results = result.split(";");
        String[] rs = results[0].split(SEPARATOR);
        String[] frs = results[1].split(SEPARATOR);
        File f = new File(ROOT_PATH + "out.txt");

        try {
            if (f.exists()) {
                f.delete();
            }
            f.createNewFile();
            FileWriter fw = new FileWriter(f);
            for (String price : rs) {
                fw.append(price.replace(".", ","));
                fw.append("\n");
            }
            fw.append("\n");
            for (String price : frs) {
                fw.append(price.replace(".", ","));
                fw.append("\n");
            }
            fw.flush();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private static void clearResults() {
        File f = new File(ROOT_PATH + "out.txt");
        if (f.exists()) {
            f.delete();
        }
    }


    public static CandleIterator getCandleIterator(int miniBatchSize,
                                                   int sequenceLength, int priceAccuracy) throws Exception {
        String fileLocation = ROOT_PATH + DATA;
        File f = new File(fileLocation);
        if (!f.exists()) throw new IOException("File does not exist: " + fileLocation);
        return new CandleIterator(
                fileLocation,
                Charset.forName("UTF-8"),
                miniBatchSize,
                sequenceLength,
                new Random(12345),
                priceAccuracy
        );
    }

    private static String[] samplePricesFromNetwork(float[] initialization, MultiLayerNetwork net,
                                                    CandleIterator iter, Random rng, int pricesToSample, int numSamples) {

        if (initialization == null) {
            initialization = new float[1];
            initialization[0] = iter.getRandomPrice();
        }

        //Create input for initialization
        INDArray initializationInput = Nd4j.zeros(numSamples, iter.inputColumns(), initialization.length);
        float[] init = initialization;
        for (int i = 0; i < init.length; i++) {
            int idx = iter.convertPriceToIndex(init[i]);
            for (int j = 0; j < numSamples; j++) {
                initializationInput.putScalar(new int[]{j, idx, i}, 1.0f);
            }
        }
        int maxStartPrices = 3;
        StringBuilder[] sb = new StringBuilder[numSamples];
        List<List<Float>> fResults = new ArrayList<>();
        for (int i = 0; i < numSamples; i++) {
            sb[i] = new StringBuilder();
            fResults.add(new ArrayList<>());
            float[] tmp;
            if(initialization.length > maxStartPrices) {
                tmp = Arrays.copyOfRange(initialization, initialization.length - maxStartPrices, initialization.length);
            } else {
                tmp = initialization;
            }
            for (float ip : tmp) {
                sb[i].append(ip).append(SEPARATOR);
            }
        }

        net.rnnClearPreviousState();
        INDArray output = net.rnnTimeStep(initializationInput);
        output = output.tensorAlongDimension((int) output.size(2) - 1, 1, 0);    //Gets the last time step output

        for (int i = 0; i < pricesToSample; i++) {
            INDArray nextInput = Nd4j.zeros(numSamples, iter.inputColumns());
            for (int s = 0; s < numSamples; s++) {
                double[] outputProbDistribution = new double[iter.totalOutcomes()];
                for (int j = 0; j < outputProbDistribution.length; j++) {
                    outputProbDistribution[j] = output.getDouble(s, j);
                }
                int sampledPriceIdx = sampleFromDistribution(outputProbDistribution, rng);

                nextInput.putScalar(new int[]{s, sampledPriceIdx}, 1.0f);
                float f = iter.convertIndexToPrice(sampledPriceIdx);//Prepare next time step input
                sb[s].append(f).append(SEPARATOR);
                fResults.get(s).add(f);
            }

            output = net.rnnTimeStep(nextInput);    //Do one time step of forward pass
        }

        String[] out = new String[numSamples];
        for (int i = 0; i < numSamples; i++) {
            sb[i].setLength(sb[i].length() - 1);

            sb[i].append(";");
            float[] futures = future();
            List<Float> fr = fResults.get(i);
            for (int j = 0; j < futures.length; j++) {
                float deltaError = futures[j] - fr.get(j);
                sb[i].append(deltaError).append(SEPARATOR);
            }
            sb[i].setLength(sb[i].length() - 1);

            String sOut = sb[i].toString();
            out[i] = sOut;
        }
        return out;
    }

    public static int sampleFromDistribution(double[] distribution, Random rng) {
        double d = 0.0;
        double sum = 0.0;
        for (int t = 0; t < 10; t++) {
            d = rng.nextDouble();
            sum = 0.0;
            for (int i = 0; i < distribution.length; i++) {
                sum += distribution[i];
                if (d <= sum) return i;
            }
            //If we haven't found the right index yet, maybe the sum is slightly
            //lower than 1 due to rounding error, so try again.
        }
        //Should be extremely unlikely to happen if distribution is a valid probability distribution
        throw new IllegalArgumentException("Distribution is invalid? d=" + d + ", sum=" + sum);
    }

    private static float[] pricesForPrediction() {
        float[] ret = {3694.5f, 3690.5f, 3690.5f, 3690f, 3688.5f, 3688.5f, 3686f, 3684.5f, 3684.5f, 3685f, 3688.5f, 3689f, 3688.5f, 3688.5f, 3689f, 3688.5f, 3688.5f, 3688.5f, 3688f, 3686.5f, 3686f, 3686.5f, 3686.5f, 3686f, 3686f, 3686f, 3686.5f, 3686.5f, 3687f, 3687.5f, 3687.5f, 3687.5f, 3694f, 3697f, 3696.5f, 3695.5f, 3695f, 3694.5f, 3694.5f, 3695f, 3695f, 3695f, 3694.5f, 3695f, 3695f, 3693.5f, 3693.5f, 3694f, 3696f, 3699.5f, 3703f, 3703f, 3701.5f, 3701f, 3700f, 3700f, 3700f, 3699.5f, 3692.5f, 3694f, 3694.5f, 3694.5f, 3695f, 3691.5f, 3691.5f, 3692f, 3691.5f, 3691.5f, 3689f, 3691f, 3691f, 3691.5f, 3693.5f, 3693f, 3693.5f, 3693f, 3693.5f, 3693f, 3691.5f, 3691.5f, 3691f, 3691.5f, 3690.5f, 3690.5f, 3689.5f, 3687f, 3687.5f, 3690.5f, 3691f, 3691.5f, 3691f, 3691.5f, 3691f, 3691f, 3690.5f, 3690f, 3690f, 3689f, 3688f, 3687f, 3686.5f, 3686.5f, 3686f, 3684.5f, 3682.5f, 3677f, 3679.5f, 3680f, 3679.5f, 3679.5f, 3680f, 3680.5f, 3680.5f, 3680f, 3680.5f, 3681f, 3681f, 3681.5f, 3681.5f, 3681.5f, 3681f, 3681f, 3681f, 3681f, 3681.5f, 3681f, 3682f, 3683.5f, 3682f, 3682f, 3681.5f, 3682f, 3681.5f, 3681.5f, 3681f, 3681f, 3681f, 3681f, 3681.5f, 3681f, 3681.5f, 3683f, 3682f, 3681.5f, 3681.5f, 3681.5f, 3680.5f, 3679f, 3674f, 3674.5f, 3676f, 3674f, 3674f, 3676f, 3674f, 3676f, 3677.5f, 3677.5f, 3678f, 3677.5f, 3678f, 3677.5f, 3678f, 3678f, 3677.5f, 3677.5f, 3677f, 3676f, 3676f, 3676.5f, 3676.5f, 3676f, 3672.5f, 3673.5f, 3674f, 3675f, 3675f, 3676.5f, 3678f, 3678.5f, 3680.5f, 3678.5f, 3679f, 3678.5f, 3679.5f, 3679.5f, 3679f, 3679.5f, 3679f, 3680f, 3680f, 3681.5f, 3684.5f, 3684f, 3684.5f, 3685f, 3685f, 3686f, 3685.5f, 3686f, 3686f, 3686f, 3685.5f, 3685.5f, 3686.5f, 3687f, 3687f, 3686.5f, 3687f, 3687f, 3686.5f, 3685.5f, 3685.5f, 3685.5f, 3685.5f, 3686f, 3685.5f, 3685.5f, 3685.5f, 3685f, 3685f, 3685f, 3684.5f, 3684.5f, 3683.5f, 3683f, 3682.5f, 3682.5f, 3682f, 3682f, 3682.5f, 3682.5f, 3683f, 3685f, 3685f, 3685.5f, 3685f, 3684.5f, 3684.5f, 3685f, 3686f, 3686f, 3686.5f, 3686.5f, 3686.5f, 3686f, 3687.5f, 3688.5f, 3690f, 3692.5f, 3692.5f, 3688.5f, 3690.5f, 3691f, 3691.5f, 3692.5f, 3691.5f, 3691.5f, 3691.5f, 3691.5f, 3691.5f, 3688.5f, 3689f, 3688f, 3685f, 3685.5f, 3686.5f, 3686.5f, 3686f, 3686.5f, 3686f, 3686.5f, 3686f, 3686f, 3686f, 3686.5f, 3686f, 3686f, 3686f, 3686f, 3685.5f, 3685.5f, 3685.5f, 3684f, 3684f, 3684.5f, 3684f, 3684f, 3684.5f, 3685.5f, 3685.5f, 3684f, 3684.5f, 3684f, 3683f, 3683.5f, 3685f, 3685f, 3685f, 3684f, 3683.5f, 3683.5f, 3684f, 3683.5f, 3684f, 3685f, 3685.5f, 3685f, 3685.5f, 3685.5f, 3685.5f, 3685f, 3685.5f, 3687.5f, 3688f, 3688.5f, 3688f, 3688f, 3688f, 3688f, 3689.5f, 3690f, 3690f, 3690.5f, 3691f, 3690.5f, 3691f, 3690.5f, 3690.5f, 3689.5f, 3689.5f, 3689f, 3689.5f, 3689.5f, 3689.5f, 3688.5f, 3688f, 3688.5f, 3688.5f, 3688.5f, 3688f, 3688f, 3687f, 3687.5f, 3687.5f, 3687.5f, 3687.5f, 3687.5f, 3689f, 3689f, 3689f, 3688.5f, 3688.5f, 3689f, 3688.5f, 3689f, 3688.5f, 3689f, 3689f, 3689f, 3689f, 3688.5f, 3688.5f, 3688.5f, 3689f, 3688.5f, 3690.5f, 3691f, 3691.5f, 3691.5f, 3691.5f, 3691f, 3691.5f, 3691f, 3691f, 3691.5f, 3691f, 3691.5f, 3691f, 3691.5f, 3691f, 3691f, 3691f, 3688.5f, 3689.5f, 3689f, 3689.5f, 3689.5f, 3689.5f, 3690.5f, 3690.5f, 3690.5f, 3690.5f, 3691f, 3691f, 3690.5f, 3691f, 3691f, 3690.5f, 3684f, 3685f, 3686f, 3686f, 3685.5f, 3687f, 3687.5f, 3687.5f, 3687f, 3689.5f, 3690f, 3690f, 3690f, 3693f, 3693.5f, 3690.5f, 3689.5f, 3689.5f, 3689f, 3689f, 3689.5f, 3690f, 3691f, 3691f, 3690.5f, 3690.5f, 3693f, 3693f, 3693f, 3693f, 3693f, 3693f, 3693.5f, 3695f, 3694.5f, 3697f, 3694f, 3715f, 3704f, 3705f, 3700f, 3699f, 3698f, 3697.5f, 3698f, 3700f, 3703.5f, 3703f, 3703f, 3702.5f, 3702.5f, 3702f, 3702f, 3701.5f, 3701f, 3700.5f, 3701f, 3700.5f, 3698.5f, 3698f, 3699f, 3699.5f, 3699f, 3699.5f, 3700f, 3700f, 3700f, 3700f, 3702f, 3702f, 3702f, 3702.5f, 3702f, 3702f, 3702.5f, 3705.5f, 3703.5f, 3703f, 3703.5f, 3703.5f, 3703f, 3703f, 3703f, 3706f, 3705f, 3704.5f, 3704f, 3704f, 3704f, 3704f, 3704f};

//        float[] ret = {3694.5f, 3690.5f, 3690.5f};
        return ret;
    }

    private static float[] future() {
        float[] ret = {3703f, 3702.5f, 3702.5f, 3703f, 3703f, 3703f, 3704f, 3706f, 3708f, 3711f};
        return ret;
    }
}
