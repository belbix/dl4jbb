package pro.belbix.dl4jbb;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.nd4j.linalg.api.ndarray.INDArray;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class UnsupervisedSet {
    List<INDArray> featuresTrain;
    List<INDArray> featuresTest;
    List<INDArray> labelsTest;
}
